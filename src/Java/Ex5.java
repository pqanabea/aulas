package Java;
import java.util.Scanner;

public class Ex5 {
	public double caixas(double c, double a, double l){
		double area = (c*a*2) + (l*a*2);
		int cx = (int)Math.round(area/1.5);
		return cx;
	}
	
	public static void main(String[] args) {
	double comp, larg, alt;
	//comp: comprimento, alt: altura
	
	comp = 0; 
	larg = 0;
	alt = 0;
	
	
	Scanner scan = new Scanner(System.in);
	System.out.println("Qual o comprimento da cozinha?");
	comp = scan.nextDouble();
	
	System.out.println("Qual a largura da cozinha?");
	larg = scan.nextDouble();
	
	System.out.println("Qual a altura da cozinha?");
	alt = scan.nextDouble();
	
	
	caixas(comp, alt, larg);
	System.out.println("Quantidade de caixas de azulejos para colocar em todas as paredes: ");
	System.out.println(caixas(comp, alt, larg));
	scan.close();
	}
}
