package Java;
import java.util.Scanner;
public class Ex4 {
	

	
	public double nlamp(double plamp, double l, double c){
		double pt = l*c*18;
		int arredondar = (int)Math.round(pt/plamp);
		return arredondar;
	}
	public static void main(String[] args) {
	double pot_lamp, larg_com, comp_com;
	
	//{pot_lamp: potencia da lampada
	//larg_com: largura do comodo
	//comp_com: comprimento do comodo
	//area_com: area do comodo
	//pot_total: potencia total
	//num_lamp: numero de lampadas}
	
	 Scanner scan = new Scanner(System.in);
	System.out.println("Qual a potencia da lampada (em watts)?");
	pot_lamp = scan.nextDouble();
	System.out.println("Qual a largura do comodo (em metros)?");
	larg_com = scan.nextDouble();
	System.out.println("Qual o comprimento do comodo (em metros)?");
	comp_com = scan.nextDouble();
	
	nlamp(pot_lamp, larg_com, comp_com);
	
	
	System.out.println("Numero de lampadas necessarias para iluminar esse comodo:" + nlamp(pot_lamp, larg_com, comp_com));
	scan.close();
	}
}
