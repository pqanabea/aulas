package Java;
import java.util.Scanner;


public class Ex2 {
	public double celsius(double tf){
		return ((tf - 32) * 5) / 9;
	}

	public static void main(String[] args) {
		double temp_f;
		temp_f = 0;		
       
       Scanner scan = new Scanner(System.in);
      
       //temp_f: temperatura fahrenheit, temp_c: temperatura celsius}
       System.out.println("Informe a temperatura em graus Fahrenheit: ");
       temp_f = scan.nextDouble();
              
       celsius(temp_f);
       System.out.println("A temperatura em graus Celsius eh:" + temp_f);
       
       scan.close();

	}

}
