package Java;
import java.util.Scanner;

public class Ex6 {
	public double media(double odomf, double odomi, double l){
		double m = (odomf - odomi) / l;
		return m;
	}
	
	public double lucro(double l, double gl, double valort){
		double lc = valort - (l * gl);
		return lc;
	}
	
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		double odom_i, odom_f, litros, valor_t, media, lucro, gasol_l;

		odom_i = 0;
		odom_f = 0;
		litros = 0;
		valor_t = 0;
		media = 0;
		lucro = 0;
		gasol_l = 1.90;

		System.out.println("Marcacao inicial do odometro (Km):");
		odom_i = scan.nextDouble();
		System.out.println("Marcacao final do odometro (Km):");
		odom_f = scan.nextDouble();
		System.out.println("Quantidade de combustivel gasto (litros):");
		litros = scan.nextDouble();
		System.out.println("Valor total recebido (R$):");
		valor_t = scan.nextDouble();

		lucro(litros, gasol_l, valor_t);
		media(odom_f, odom_i, litros);
		
		System.out.printf("Media de consumo em Km/L: %.1f ", + media(odom_f, odom_i, litros));

		System.out.printf("Lucro (liquido) do dia: R$ %.2f", + lucro(litros, gasol_l, valor_t);

		scan.close();

	}

}
